package com.examples.client;


import com.examples.domain.CatFacts;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "cat-service", url="https://cat-fact.herokuapp.com")
public interface CatFactsClient {

    @GetMapping("/facts/random?animal_type=cat&amount=1")
    CatFacts getCatFacts();
}
