package com.examples.utils;

import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvParser;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;

@Slf4j
public class DataProcessorUtils {

    public static <T> List<T> readData(Class<T> clazz, String fileName) {
        CsvMapper mapper = new CsvMapper();
        mapper.enable(CsvParser.Feature.TRIM_SPACES);
        CsvSchema schema = mapper.schemaFor(clazz)
                .withHeader()
                .withArrayElementSeparator(",")
                .withColumnSeparator(';')
                .withColumnReordering(true);
        ObjectReader reader = mapper.readerFor(clazz).with(schema);
        try {
            return reader.<T>readValues(getFileStream(fileName)).readAll();
        } catch (IOException exception) {
            log.warn("Csv file parsing failed {}", exception.getMessage());
        }
        return Collections.emptyList();
    }

    private static InputStream getFileStream(String fileName) {
        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        return classLoader.getResourceAsStream("data/".concat(fileName));
    }
}
