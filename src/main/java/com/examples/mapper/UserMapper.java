package com.examples.mapper;

import com.examples.domain.UserDTO;
import com.examples.model.User;

public class UserMapper {
    public static UserDTO mapToUserDto(User user) {
        return UserDTO.builder()
                .id(user.getId())
                .email(user.getEmail())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .build();
    }
}
