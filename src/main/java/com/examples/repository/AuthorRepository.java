package com.examples.repository;

import com.examples.domain.Author;
import com.examples.domain.PixelView;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.examples.utils.DataProcessorUtils.readData;

@Repository
@RequiredArgsConstructor
public class AuthorRepository {

    @Cacheable("authors")
    public List<Author> getAllAuthors() {
        return readData(Author.class, "authors.csv");
    }

    public static void main(String[] args) throws JsonProcessingException {

        List<PixelView> pixelViews = readData(PixelView.class, "authors.csv");
        System.out.println(new ObjectMapper().writeValueAsString(pixelViews));
    }
}
