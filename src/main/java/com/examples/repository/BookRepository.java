package com.examples.repository;

import com.examples.domain.BookDetails;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.examples.utils.DataProcessorUtils.readData;

@Repository
public class BookRepository {

    @Cacheable("books")
    public List<BookDetails> getAllBooks() {
        List<BookDetails> books = readData(BookDetails.class, "books.csv");
        List<BookDetails> magazines = readData(BookDetails.class, "magazines.csv");
        magazines.forEach(magazine -> magazine.setMagazine(true));
        return Stream.of(books, magazines)
                .flatMap(List::stream)
                .distinct()
                .collect(Collectors.toList());
    }
}
