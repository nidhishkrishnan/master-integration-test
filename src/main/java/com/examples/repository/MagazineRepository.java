package com.examples.repository;

import com.examples.domain.Magazine;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.examples.utils.DataProcessorUtils.readData;

@Repository
public class MagazineRepository {

    @Cacheable("magazines")
    public List<Magazine> getAllMagazines() {
        return readData(Magazine.class, "magazines.csv");
    }
}
