package com.examples.gateway;

import com.examples.client.CatFactsClient;
import com.examples.domain.CatFacts;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CatGateway {

    private final CatFactsClient catFactsClient;

    public CatFacts getCatFacts() {
        return catFactsClient.getCatFacts();
    }
}
