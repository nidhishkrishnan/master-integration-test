package com.examples.gateway;

import com.examples.client.DogClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DogGateway {

    private final DogClient dogClient;

    public String getDogImages() {
        return dogClient.getDogImages().getMessage();
    }
}
