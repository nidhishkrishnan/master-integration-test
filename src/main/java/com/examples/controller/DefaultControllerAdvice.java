package com.examples.controller;

import com.examples.domain.ErrorResponse;
import com.examples.exception.EntityNotFoundException;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.AbstractMappingJacksonResponseBodyAdvice;

import static com.google.common.collect.Lists.newArrayList;

@RestControllerAdvice
public class DefaultControllerAdvice extends AbstractMappingJacksonResponseBodyAdvice {


    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleAllOtherException(final Exception exception) {
        ErrorResponse apiError = new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Error Occurred" , newArrayList(exception.getLocalizedMessage()));
        return new ResponseEntity<>(apiError, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler({ EntityNotFoundException.class })
    public ResponseEntity<Object> handleAllEntityNotFoundException(final Exception exception) {
        ErrorResponse apiError = new ErrorResponse(HttpStatus.NOT_FOUND.value(), "Not Found" , newArrayList(exception.getLocalizedMessage()));
        return new ResponseEntity<>(apiError, HttpStatus.NOT_FOUND);
    }

    @Override
    protected void beforeBodyWriteInternal(MappingJacksonValue bodyContainer, MediaType contentType, MethodParameter returnType, ServerHttpRequest request, ServerHttpResponse response) {
//        if (isExcluded(request.getURI())){
//            return;
//        }
//
//        if (shouldNotContainUserData(request.getURI())) {
//            ResponseWrapper wrapper = new ResponseWrapper(bodyContainer.getValue(), null);
//            bodyContainer.setValue(wrapper);
//            return;
//        }
//
//        UserData u = getUserData();
//        ResponseWrapper wrapper = new ResponseWrapper(bodyContainer.getValue(), u);
        bodyContainer.setValue(bodyContainer.getValue());
    }
}


