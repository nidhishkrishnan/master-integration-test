package com.examples.controller;

import com.examples.domain.Magazine;
import com.examples.service.MagazineService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("v1/magazines")
public class MagazineController {

    private final MagazineService magazineService;

    @GetMapping("/")
    public List<Magazine> magazines() {
        return magazineService.getMagazines();
    }

    @GetMapping("/isbn/{isbn}")
    public Magazine findMagazineByIsbn(@PathVariable String isbn) {
        return magazineService.findMagazineByIsbn(isbn);
    }

    @GetMapping("/email-id/{emailId}")
    public List<Magazine> findMagazinesByEmail(@PathVariable String emailId) {
        return magazineService.findMagazinesByEmail(emailId);
    }
}
