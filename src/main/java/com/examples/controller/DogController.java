package com.examples.controller;

import com.examples.service.DogService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("v1/dog/")
@RequiredArgsConstructor
public class DogController {

    private final DogService dogService;

    @Value("${test.name:}")
    String name;

//    @Autowired
//    RestTemplate restTemplate;

    @GetMapping("images")
    public List<String> getDogImages() {
        log.info("name====>{}",name);
        return dogService.getDogImages();
    }

//    @RequestMapping("/list")
//    public String list() {
//        HttpHeaders httpHeaders = new HttpHeaders();
//        httpHeaders.add("Authorization", "Bearer VALID_TOKEN");
//        HttpEntity requestEntity = new HttpEntity(httpHeaders);
//        ResponseEntity<String> response = restTemplate
//                .exchange("http://library-service/gameslist", HttpMethod.GET, requestEntity,
//                        String.class);
//        return response.getBody();
//    }
}
