package com.examples.controller;

import com.examples.domain.BookDetails;
import com.examples.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("v1/books")
public class BookController {

    private final BookService bookService;

    @GetMapping("/")
    public List<BookDetails> books() {
        return bookService.getBooks();
    }

    @GetMapping("/isbn/{isbn}")
    public BookDetails findBookByIsbn(@PathVariable String isbn) {
        return bookService.findBookByIsbn(isbn);
    }

    @GetMapping("/email-id/{emailId}")
    public List<BookDetails> findBooksByEmail(@PathVariable String emailId) {
        return bookService.findBooksByEmail(emailId);
    }

    @GetMapping("/sort")
    public List<BookDetails> sortByTitle() {
        return bookService.sortByTitle();
    }
}
