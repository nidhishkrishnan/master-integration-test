package com.examples.controller;

import com.examples.domain.Author;
import com.examples.service.AuthorService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("v1/magazines")
public class AuthorController {

    private final AuthorService authorService;

    @GetMapping("authors")
    public List<Author> authors() {
        return authorService.getAuthors();
    }
}
