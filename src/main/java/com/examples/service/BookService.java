package com.examples.service;

import com.examples.domain.BookDetails;
import com.examples.exception.EntityNotFoundException;
import com.examples.repository.BookRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BookService {

    private final BookRepository bookRepository;

    public List<BookDetails> getBooks() {
        return bookRepository.getAllBooks();
    }

    public BookDetails findBookByIsbn(final String isbn) {
        return getBooks().stream().filter(book -> book.getIsbn().equals(isbn)).findAny().orElseThrow(()-> new EntityNotFoundException(String.format("Book not found for the given isbn %s", isbn)));
    }

    public List<BookDetails> findBooksByEmail(String emailId) {
        return getBooks().stream().filter(book -> book.getAuthors().contains(emailId)).collect(Collectors.toList());
    }

    public List<BookDetails> sortByTitle() {
        return getBooks().stream().sorted(Comparator.comparing(BookDetails::getTitle)).collect(Collectors.toList());
    }
}
