package com.examples.service;

import com.examples.domain.Magazine;
import com.examples.exception.EntityNotFoundException;
import com.examples.repository.MagazineRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MagazineService {

    private final MagazineRepository magazineRepository;

    public List<Magazine> getMagazines() {
        return magazineRepository.getAllMagazines();
    }

    public Magazine findMagazineByIsbn(final String isbn) {
        return getMagazines().stream().filter(book -> book.getIsbn().equals(isbn)).findAny().orElseThrow(()-> new EntityNotFoundException(String.format("Magazines not found for the given isbn %s", isbn)));
    }

    public List<Magazine> findMagazinesByEmail(String emailId) {
        return getMagazines().stream().filter(book -> book.getAuthors().contains(emailId)).collect(Collectors.toList());
    }
}
