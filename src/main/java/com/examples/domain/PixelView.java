package com.examples.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.Set;

@Setter
@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class PixelView  {

    private Long id;
    private String location;
    private Set<String> referer;
    private String value;
    private String cid;
    private String position;
    private String ageGroups;
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date df;
    private int prio;


}
