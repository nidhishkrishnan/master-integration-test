package com.examples.domain;

import lombok.Data;

@Data
public class DogApiResponse {

    private String message;

    private String status;
}
