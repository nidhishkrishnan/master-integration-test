package com.examples.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Book extends Publication {

    private String description;

    @JsonFormat(pattern="dd.MM.yyyy")
    private Date publishedAt;
}
