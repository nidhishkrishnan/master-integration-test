package com.examples.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
public class Magazine extends Publication {

    @JsonFormat(pattern="dd.MM.yyyy")
    private Date publishedAt;
}
