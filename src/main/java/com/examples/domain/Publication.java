package com.examples.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class Publication {

    private String title;

    private String isbn;

    private List<String> authors;
}
