package com.examples.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Setter
@Getter
@EqualsAndHashCode(of = "isbn")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BookDetails {

    private String title;

    private String isbn;

    private List<String> authors;

    private String description;

    @JsonProperty("isMagazine")
    private boolean isMagazine = false;

    @JsonFormat(pattern="dd.MM.yyyy")
    private Date publishedAt;
}
