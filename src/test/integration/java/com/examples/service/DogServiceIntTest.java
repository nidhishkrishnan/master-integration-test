package com.examples.service;

import com.examples.config.AbstractIT;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

class DogServiceIntTest extends AbstractIT {

    @Autowired
    private DogService dogService;

    @Test
    void getDogImagesTest() {

        List<String> dogImages = dogService.getDogImages();
        assertThat(dogImages, hasSize(250));
    }
}
