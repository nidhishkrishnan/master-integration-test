package com.examples.controller;

import com.examples.config.AbstractIT;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class DogControllerIT extends AbstractIT {

    @Autowired
    private TestRestTemplate template;

    @Test
    void getDogImagesTest() {
        ResponseEntity<List> response = this.template.getForEntity("/v1/dog/images", List.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).hasSize(250);
    }
}

