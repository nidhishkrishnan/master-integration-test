package com.examples;

import com.examples.controller.DogController;
import com.examples.service.DogService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
class DogControllerAT {

    private MockMvc mockMvc;

    @InjectMocks
    private DogController dogController;

    @Mock
    private DogService dogService;

    @BeforeEach
    void setMockOutput()
    {
        mockMvc = MockMvcBuilders.standaloneSetup(dogController).build();
    }

    @Test
    @DisplayName("Test for getting Dog images list ('v1/dog/images/')")
    void getDogImagesTest() throws Exception
    {
        when(dogService.getDogImages()).thenReturn(buildDogImages());
        mockMvc.perform(MockMvcRequestBuilders.get("/v1/dog/images/")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)));
    }

    private List<String> buildDogImages() {
        return newArrayList("https://images.dog.ceo/breeds/retriever-golden/leo_small.jpg",
                "https://images.dog.ceo/breeds/pembroke/n02113023_13200.jpg",
                "https://images.dog.ceo/breeds/leonberg/n02111129_5144.jpg");
    }
}

